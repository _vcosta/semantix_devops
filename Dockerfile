FROM debian:10

LABEL maintainer "vinicius.costa.92@gmail.com"

RUN apt update && \
    apt install -y \
            python3.7 \
            python3-pip

COPY ./requirements.txt /conf/

RUN pip3 install --requirement /conf/requirements.txt

RUN rm -rf /var/lib/apt/lists/*

COPY ./code/ /code/

WORKDIR /code/

RUN useradd -ms /bin/bash semantix
USER semantix

CMD ["python3", "app.py"]

EXPOSE 4000
