# DEVOPS HOME TEST - Semantix


## Execução da API

Para executar a solução é necessário ter instalado o Docker em sua máquina.
(Guias de instalação: https://docs.docker.com/glossary/?term=installation)

Os arquivos presentes na pasta /code são utilizados pelo Jenkins e disponibilizados no registry desse repositório, nenhum processo de build local é necessário.

Para rodar a solução a partir da imagem mais recente no registry:
```
docker run -d -p 4000:4000 registry.gitlab.com/dime.nicius/semantix_devops:latest
```

Para fazer todo o processo no ambiente local, entre na pasta da solução e execute os seguintes comandos:
```
docker build -t semantix_devops .

docker run -d -p 4000:4000 semantix_devops
```
A execução também pode ser feita através do docker-compose com o comando:

```
docker-compose up app
```

## Jenkins Pipeline

Para subir o ambiente do Jenkins, existe um arquivo docker-compose com as definições explicitadas abaixo. Basta executar o comando ``docker-compose up jenkins`` na pasta da solução.
``É necessário ter o docker-compose instalado na máquina.``
(Guia de instalação: https://docs.docker.com/compose/install/)

Caso não tenha o docker-compose instalado:
```
 docker run -v $(pwd)/jenkins_home:/var/jenkins_home \
            -v /var/run/docker.sock:/var/run/docker.sock \
            -v /usr/bin/docker:/usr/bin/docker \
            -p 8080:8080 \
            -p 50000:50000 \
            --name semantix_devops_jenkins_1
            --user root \
            jenkins/jenkins:lts
```

O serviço pode ser acessado apartir de http://localhost:8080.
Uma chave será pedida na primeira vez que acessar, para obtê-la do container basta executar o comando:

```
docker exec semantix_devops_jenkins_1 cat /var/jenkins_home/secrets/initialAdminPassword
```

Siga instalando os plugins sugeridos e aguarde sua conclusão. Então crie uma conta de administrador e continue até o painel de controle.

Com a instalação concluída crie um pipeline novo e na área **Pipeline**, selecione ``Definition: Pipeline script``, e então adicione o código contido em ``Pipeline/Jenkinsfile``.

Agora é preciso instalar a  configurar as credenciais do GitLab para publicação de uma nova imagem no registry.
Em **Credentials > System > Global Credentials** crie uma nova credential com as seguintes informações:
- Username: dimenicius_jenkins
- Password: semantix@2019
- ID: gitlab_registry

Agora é só executar o Pipeline clicando em Build Now no menu lateral.

### Notas

Algumas observações:

 - Para execução dos comandos docker dentro do container do Jenkins é necessário o mapeamento de "/var/run/docker.sock" e "/usr/bin/docker:/usr/bin/docker" para que ele consiga utilizar o docker do host.

 - Assim como o passo anterior, para execução dos comandos docker funcionar, foi necessário executá-los como root. Apesar dessa não ser uma opção recomendada, realizei o procedimento desta forma para que a criação de infraestrutura do ambiente Jenkins não tomasse mais tempo.

 - Para publicação da imagem no GitLab foi necessária a troca do storage-driver para overlay.
 https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-storage-driver

 - (OPCIONAL) A pasta /jenkins_home é utilizada apenas para persistir as configurações do container.


## Refatoramento da API

Migrei a solução inicial para o [Sanic](https://sanicframework.org/) devido a familiaridade com Flask. Ainda está escrita em Python e foi possível obter alguns ganhos sem alterar a lógica da aplicação.

Para teste de performance do processo de requisições, criei um script python (get_urls.py) para substituir o comando ``curl < queries`` utilizado nos testes. Sendo possível de realizar as requisições na API também de forma assíncrona, assim foi possível ter mais um pouco de ganho no tempo de respostas para grandes listas.

Algumas outras abordagens foram feitas mas não foram obtidos ganhos significativos, como por exemplo:

- Busca na lista em threads paralelas
- Buscas asincronas em partes diferentes da lista
- Utilização de um Pandas DataFrame e suas funções (Piorou em muito o tempo de resposta)

## Aplicação de consumo de API

Também está presente no docker-compose.yml um container que executa as queries de consulta na api.
A solução se baseia na execução assincrona de requisições contidas no arquivo ('queries.txt'), as queries foram alteradas para porta **4000** do container **app**.
Sua execução é controlada por um script [wait-for-it.sh](https://github.com/vishnubob/wait-for-it) que só libera a execução do script ``get_urls.py`` quando tiver uma resposta de http://app:4000 .

Para executar o container com as requisições, utilize o comando:
```
docker-compose up queries
```

Caso não possua docker-compose:
```
docker run registry.gitlab.com/dime.nicius/semantix_devops:latest ./wait-for-it.sh -h app -p 4000 -- python3 get_urls.py
```
