from sanic import Sanic
from sanic.response import json

app = Sanic()

def initdb(filename="data.json"):
    import json
    db = DB()
    with open(filename, "r") as f:
        for l in f:
            q_dict = json.loads(l)
            for e in q_dict["data"]:
                db.add((e['review_id'], e['date'], e['message']))
    return db


class DB(object):
    def __init__(self, entries = None):
        self.entries = [] if entries is None else entries
    async def search(self, query_string):
        terms = tuple(query_string.split())
        return list(filter(lambda x: all(t in x[2] for t in terms), self.entries))
      
    def add(self, entry):
        "An entry is a tuple of (id, datatime, text)."
        self.entries.append(entry)


@app.route('/s')
def no_search(request):
    return json(dict(size = 0,  entries = []))

@app.route('/s/<search>')
async def search(request, search):
    global db
    r = await db.search(search)
    return json(dict(size = len(r),  entries = r))

if __name__ == '__main__':
    db = initdb()
    app.run(host='0.0.0.0', port=4000, access_log=False)
