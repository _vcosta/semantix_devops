import time
import asyncio 
import aiohttp

URL_LIST = open("queries.txt").readlines() 

async def get_url(session, url):
    async with session.get(url) as resp:
        return await resp.read()

async def download_one(session, url):
    response = await get_url(session, url)
    return response

async def download_many(url_list):
    async with aiohttp.ClientSession() as session:
        res = await asyncio.gather(                
            *[asyncio.create_task(download_one(session, url))
                for url in url_list])

    return len(res)

def main():
    t0 = time.time()
    count = asyncio.run(download_many(URL_LIST))
    elapsed = time.time() - t0
    print('{} Requests made in {:.3f}s'.format(count, elapsed))

if __name__ == '__main__':
    main()